package com.abi.thym.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AppController {
	
	@GetMapping("/inicio")
	public String index(Model model) {
		model.addAttribute("mensaje", "Demo for Spring Boot, Thymeleaf adn Actuators");
		return "index";
	}
	
}
